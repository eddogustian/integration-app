<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\API\RajaOngkirController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/provinces', [RajaOngkirController::class, 'getProvinces']);
Route::get('/cities/{id}', [RajaOngkirController::class, 'getCities']);
Route::post('/checkOngkir', [RajaOngkirController::class, 'checkOngkir']);
