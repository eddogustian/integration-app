<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CheckOngkirController;


Route::get('/', [CheckOngkirController::class, 'index'])->name('index');
Route::post('/', [CheckOngkirController::class, 'check_ongkir'])->name('check-ongkir');
Route::get('/cities/{province_id}', [CheckOngkirController::class, 'getCities'])->name('get-cities');